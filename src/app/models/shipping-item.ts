export interface ShippingItem {
  type: string;
  price: number;
}
