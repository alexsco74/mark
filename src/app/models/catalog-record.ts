export interface CatalogRecord {
  id: number;
  parentId: number;
  label: string;
  icon?: string;
  path?: string;
}
