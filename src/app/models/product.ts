import {CatalogRecord} from "./catalog-record";

export interface Product {
  id: number;
  name: string;
  description: string;
  price: number;
  catalog: CatalogRecord;
}
