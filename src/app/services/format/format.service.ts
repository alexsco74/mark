import {Injectable} from '@angular/core';
import {CurrencyPipe} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  constructor(private currencyPipe: CurrencyPipe) {
  }

  currency(value, currencyCode = 'RUR', display = "symbol-narrow", digitsInfo = "0.0") {
    return this.currencyPipe.transform(value, currencyCode, display, digitsInfo);
  }
}
