import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MenuItem} from 'primeng/api';
import {CatalogRecord} from '../models/catalog-record';
import {Product} from '../models/product';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) {
  }

  getCatalogTree(): Observable<any> {
    return this.httpClient.get<MenuItem[]>('/assets/catalog-tree.json');
  }

  getCatalogRecords(): Observable<any> {
    return this.httpClient.get<CatalogRecord[]>('/assets/catalog-records.json');
  }

  getProductList($catalogPath?: string): Observable<any> {
    return this.httpClient.get<Product[]>('/assets/products.json');
  }

  getLogo(): Observable<any> {
    const options = {responseType: 'text' as 'json'};
    const file = '/assets/img/logo.svg';
    return this.httpClient.get<string>(file, options);
  }
}
