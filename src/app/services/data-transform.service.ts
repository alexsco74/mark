import {Injectable} from '@angular/core';
import {CatalogRecord} from "../models/catalog-record";
import {MenuItem} from "primeng/api";

@Injectable({
  providedIn: 'root'
})
export class DataTransformService {

  constructor() {
  }

  getCatalogMenuFromRecords(records: CatalogRecord[]): MenuItem[] {
    let catalogMenu: MenuItem[] = [];
    let catalogMenuParents = this.getCatalogMenuParents(records);
    catalogMenu = this.getCatalogMenuFromParents(catalogMenuParents, catalogMenuParents[0]);
    return catalogMenu;
  }

  private getCatalogMenuParents(records: CatalogRecord[]): {} {
    const catalogMenuParents = {};
    for (const record of records) {
      if (!catalogMenuParents[record.parentId]) {
        catalogMenuParents[record.parentId] = [];
      }
      catalogMenuParents[record.parentId].push(record);
    }
    return catalogMenuParents;
  }

  private getCatalogMenuFromParents(catalogMenuParents, records: CatalogRecord[]) {
    let children = [];
    for (let record of records) {
      let menuItem: MenuItem = {
        id: String(record.id),
        label: record.label,
        icon: record.icon || undefined,
        routerLink: record.path ? ['catalog', record.path] : undefined,
        items: undefined
      };
      if (catalogMenuParents[record.id]) {
        menuItem.items = this.getCatalogMenuFromParents(catalogMenuParents, catalogMenuParents[record.id]);
      }
      children.push(menuItem);
    }
    return children;
  }
}
