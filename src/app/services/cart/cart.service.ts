import {Injectable} from '@angular/core';
import {Product} from '../../models/product';
import {CartItem} from "../../models/cart-item";
import {HttpClient} from "@angular/common/http";
import {ShippingItem} from "../../models/shipping-item";

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: CartItem[] = [];

  constructor(private httpClient: HttpClient) {
  }

  getShippingPrice() {
    return this.httpClient.get<ShippingItem[]>('/assets/shipping.json');
  }

  addToCart(product: Product, quantity = 1): void {
    this.items.push({product, quantity});
  }

  getItems(): CartItem[] {
    return this.items;
  }

  clearCart(): CartItem[] {
    this.items = [];
    return this.items;
  }

  deleteItem(i: number) {
    this.items.splice(i, 1);
  }
}
