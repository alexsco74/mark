import {NgModule} from '@angular/core';
import {CartService} from "./cart/cart.service";
import {FormatService} from "./format/format.service";

@NgModule({
  providers: [
    CartService,
    FormatService
  ]
})
export class ServicesModule {
}
