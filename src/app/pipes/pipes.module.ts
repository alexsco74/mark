import {NgModule} from '@angular/core';
import {CurrencyPipe} from '@angular/common';

@NgModule({
  providers: [
    CurrencyPipe
  ],
})
export class PipesModule {
}
