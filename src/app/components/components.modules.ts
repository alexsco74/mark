import {NgModule} from '@angular/core';
import {ProductListComponent} from './product-list/product-list.component';
import {RouterModule} from "@angular/router";
import {ProductDetailsComponent} from "./product-details/product-details.component";
import {ProductItemComponent} from './product-item/product-item.component';
import {ProductAlertsComponent} from './product-alerts/product-alerts.component';
import {CartComponent} from './cart/cart.component';
import {TopBarComponent} from "./top-bar/top-bar.component";
import {CommonModule} from "@angular/common";
import {ShippingComponent} from './shipping/shipping.component';
import {MenubarModule} from "primeng/menubar";
import {SharedModule} from "primeng/api";
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';

@NgModule({
  declarations: [
    CartComponent,
    ProductListComponent,
    ProductItemComponent,
    ProductAlertsComponent,
    ProductDetailsComponent,
    TopBarComponent,
    ShippingComponent
  ],
  exports: [
    ProductListComponent,
    ProductItemComponent,
    ProductAlertsComponent,
    ProductDetailsComponent,
    CartComponent,
    TopBarComponent,
    ButtonModule,
    InputTextModule
  ],
  imports: [
    RouterModule.forRoot([
      {path: '', component: ProductListComponent},
      {path: 'products/:productId', component: ProductDetailsComponent},
      {path: 'cart', component: CartComponent},
      {path: 'shipping', component: ShippingComponent},
      {path: 'catalog/:catalogPath', component: ProductListComponent},
    ]),
    CommonModule,
    MenubarModule,
    SharedModule,
    ButtonModule,
    InputTextModule
  ]
})
export class ComponentsModule {
}
