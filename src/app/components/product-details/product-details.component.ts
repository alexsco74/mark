import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../models/product';
import {CartService} from '../../services/cart/cart.service';
import {HttpService} from "../../services/http.service";

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  product: Product;

  constructor(private route: ActivatedRoute,
              private cartService: CartService,
              private httpService: HttpService) {
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const productIdFromRoute = Number(routeParams.get('productId'));
    this.httpService.getProductList().subscribe(
      (products) => {
        this.product = products.find(product => product.id === productIdFromRoute);
      },
      (e) => {
        window.alert('Сервер не доступен. Попробуйте позже.');
      }
    );
  }
  addToCart(product:  Product): void {
    this.cartService.addToCart(product);
    window.alert('Товар добавлен в корзину');
  }
}
