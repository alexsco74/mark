import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../models/product';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input() product: Product;

  constructor() {
  }

  ngOnInit(): void {
  }

  share(): void {
    alert('К продукту предоставлен доступ');
  }

  showAlertOnNotify(): void {
    alert('Вы получите уведомление когда товар появится в продаже');
  }

}
