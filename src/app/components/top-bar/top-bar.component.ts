import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/api';
import {HttpService} from '../../services/http.service';
import {DataTransformService} from '../../services/data-transform.service';
import {tap} from 'rxjs/operators';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  items: MenuItem[];
  logo: SafeHtml;

  constructor(private httpService: HttpService, private dataTransformService: DataTransformService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.httpService.getLogo().subscribe(
      (logo) => {
        this.logo = this.sanitizer.bypassSecurityTrustHtml(logo);
      },
      (e) => {
        console.warn('Невозможно загрузить логотип.', e);
      }
    );
    this.httpService.getCatalogRecords().subscribe(
      (result) => {
        this.items = this.dataTransformService.getCatalogMenuFromRecords(result);
      },
      (e) => {
        window.alert('Сервер не доступен. Попробуйте позже.');
      }
    );
  }

}
