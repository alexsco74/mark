import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../services/http.service';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../models/product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})

export class ProductListComponent implements OnInit {
  products: Product[];
  constructor(private route: ActivatedRoute, private httpService: HttpService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const catalogPathFromRoute = routeParams.get('catalogPath');
    this.httpService.getProductList(catalogPathFromRoute).subscribe(
      (products) => {
        this.products = products;
      },
      (e) => {
        window.alert('Сервер не доступен. Попробуйте позже.');
      }
    );

  }

}
