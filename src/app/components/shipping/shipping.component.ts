import {Component, OnInit} from '@angular/core';
import {CartService} from "../../services/cart/cart.service";
import {ShippingItem} from "../../models/shipping-item";
import {FormatService} from "../../services/format/format.service";

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit {

  shippingItems: ShippingItem[];

  constructor(private cartService: CartService,
              private formatService: FormatService) {
  }

  ngOnInit(): void {
    this.getShippingItems();
  }

  getShippingItems() {
    this.cartService.getShippingPrice().subscribe(
      (result) => {
        this.shippingItems = result;
      },
      (e) => {
        window.alert('Сервер не доступен. Попробуйте позже.');
      }
    );
  }

  getPrice(price: number) {
    return this.formatService.currency(price);
  }
}
