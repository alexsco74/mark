import {Component, OnInit} from '@angular/core';
import {CartService} from '../../services/cart/cart.service';
import {CartItem} from "../../models/cart-item";
import {FormatService} from "../../services/format/format.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  items: CartItem[] = this.cartService.getItems();

  constructor(private cartService: CartService,
              private formatService: FormatService) {
  }

  ngOnInit(): void {
  }

  getSum(item: CartItem): string {
    return this.formatService.currency(item.product.price * item.quantity);
  }

  getPrice(price: number): string {
    return this.formatService.currency(price);
  }

  deleteItem(i: number): void {
    this.cartService.deleteItem(i);
    window.alert('Товар удален из корзины.');
  }
}
